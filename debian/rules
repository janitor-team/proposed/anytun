#!/usr/bin/make -f
# See debhelper(7) (uncomment to enable)
# output every command that modifies files on the build system.
#export DH_VERBOSE = 1

include /usr/share/dpkg/architecture.mk

# see FEATURE AREAS in dpkg-buildflags(1)
export DEB_BUILD_MAINT_OPTIONS = hardening=+all

# see ENVIRONMENT in dpkg-buildflags(1)
# package maintainers to append CFLAGS
#export DEB_CFLAGS_MAINT_APPEND  = -Wall -pedantic
# package maintainers to append LDFLAGS
#export DEB_LDFLAGS_MAINT_APPEND = -Wl,--as-needed


configure: configure-stamp
configure-stamp:
	dh_testdir
	cd src && ./configure --no-manpage --prefix=/usr --sysconfdir=/etc --examplesdir=/usr/share/anytun/examples --cross-prefix=${DEB_HOST_GNU_TYPE}- && cd ..
	touch configure-stamp

build: build-arch build-indep

build-arch: build-stamp
build-indep: build-stamp

build-stamp: configure-stamp
	dh_testdir

	cd src && $(MAKE) && cd ..

	touch $@

clean:
	dh_testdir
	dh_testroot
	rm -f build-stamp configure-stamp

	# Add here commands to clean up after the build process.
	$(MAKE) --directory=$(CURDIR)/src distclean
	dh_clean

install: build
	dh_testdir
	dh_testroot
	dh_installdirs

	# Add here commands to install the package into debian/anytun.
	cd src && $(MAKE) DESTDIR=$(CURDIR)/debian/anytun install && cd ..
	# Fix unusual path:
	mv debian/anytun/usr/share/anytun/examples/anytun debian/anytun/usr/share/anytun/examples/config
	echo "example configurations can be found at /usr/share/anytun/examples/config" > debian/anytun/etc/anytun/README

# Build architecture-independent files here.
binary-indep: install
# We have nothing to do by default.

# Build architecture-dependent files here.
binary-arch: install
	dh_testdir
	dh_testroot
	dh_installchangelogs ChangeLog
	dh_installdocs
	dh_installman doc/anytun.8 doc/anytun-config.8 doc/anytun-controld.8 doc/anytun-showtables.8
	dh_installexamples
	dh_installsystemd anytun.service
	dh_installinit
	dh_link
	dh_strip
	dh_compress
	dh_lintian
	dh_fixperms
	find debian/anytun -type f -name config     -exec chmod 600 {} \;
	find debian/anytun -type f -name post-up.sh -exec chmod 755 {} \;
	chmod 600 debian/anytun/usr/share/anytun/examples/config/server/conf.d/*
	dh_installdeb
	dh_shlibdeps
	dh_gencontrol
	dh_md5sums
	dh_builddeb

binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary install configure
